﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace CegesAutok.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class CegesAutokV3Controller : ControllerBase
{
    private static readonly string FilePath = Path.Combine(
        Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!,
        "autok.txt");
    
    [HttpGet]
    public async Task<string> GetEmployeeRecords(string employeeId)
    {
        string[] fileLines = await System.IO.File.ReadAllLinesAsync(FilePath);
        
        var filteredLines = fileLines
            .Where(line => BelongsToEmployee(line, employeeId));
        
        return string.Join(Environment.NewLine, filteredLines);
    }

    private static bool BelongsToEmployee(string line, string employeeId)
    {
        string[] lineSegments = line.Split(' ');
        string currentEmployeeId = lineSegments[3];
        return currentEmployeeId == employeeId;
    }
}