﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace CegesAutok.Controllers;

/**
Az autok.txt  fájl egy hónap (30 nap) adatait rögzíti. Egy sorban szóközökkel
elválasztva 6 adat található az alábbi sorrendben:

nap                  egész szám (1-30)           a hónap adott napja 
óra:perc             szöveg (óó:pp formátumban)  a ki- vagy a behajtás időpontja  
rendszám             6 karakteres szöveg         az autó rendszáma 
személy azonosítója  egész szám (500-600)        autót igénybe vevő dolgozó id-ja 
km számláló          egész szám                  a km számláló állása 
ki/be hajtás         egész szám (0 vagy 1)       parkolóból kihajtás: 0, behajtás: 1

Példa sor:
1 17:03 CEG306 523 24287 0

- Feladat:
Készíts egy API endpoint-ot, ami a megadott ID-jú személy összes rekordját visszaadja 
az eredeti formában (tehát szövegesen)!

URL formátum: GET {host}/CegesAutokV1/GetEmployeeRecords?employeeId=501

 */
[ApiController]
[Route("[controller]/[action]")]
public class CegesAutokV1Controller : ControllerBase
{
    private static readonly string FilePath = Path.Combine(
        Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!,
        "autok.txt");
    
    // TODO ;-)
}