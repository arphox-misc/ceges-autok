﻿using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace CegesAutok.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class CegesAutokV2Controller : ControllerBase
{
    private static readonly string FilePath = Path.Combine(
        Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!,
        "autok.txt");
    
    [HttpGet]
    public string GetEmployeeRecords(int employeeId)
    {
        List<string> lines = new List<string>();
        StringBuilder sb = new StringBuilder();
        lines = System.IO.File.ReadAllText(FilePath).Split("\r\n").ToList();
        for (int i =0; i< lines.Count();i++)
        {
            if (lines[i]== "") continue;
            if (int.Parse(lines[i].Split(" ")[3]) == employeeId)
            {
              sb.Append(lines[i] + "\r\n");
            }
        }
        return sb.ToString();
    }
}