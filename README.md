# Céges autók

- [Feladatlap link (e_inf_19maj_fl.pdf)](https://dload-oktatas.educatio.hu/erettsegi/feladatok_2019tavasz_emelt/e_inf_19maj_fl.pdf), de egyébként a feladatok könyvtárban is megtalálható.
- [Forrásfájlok link (e_inffor_19maj_fl.zip)](https://www.oktatas.hu/bin/content/dload/erettsegi/feladatok_2019tavasz_emelt/e_inffor_19maj_fl.zip), de egyébként a feladatok könyvtárban is megtalálható az `autok.txt` fájl.
